package com.iceberg.lctrain2;

public class MemStatItem {

	public static final String separator = ":";

	private int uniqueIndex;

	private long time;

	private boolean isSuccess;

	public MemStatItem(int uniqueIndex, long time, boolean isSuccess) {
		super();
		this.uniqueIndex = uniqueIndex;
		this.time = time;
		this.isSuccess = isSuccess;
	}

	public String toDataString() {
		return uniqueIndex + separator + (isSuccess ? "1" : "0") + separator
				+ time;
	}

}
