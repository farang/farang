package com.iceberg.lctrain2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.iceberg.tlm.alphabet.Symbol;

public class Helper {

	public static final String dbDateTimeFormat = "yyyy:MM:dd HH:mm:ss";

	public static final String ALPHABET = "alphabet";

	public static final String CONSONANT = "consonant";

	public static final String VOWEL = "vowel";

	public static final String UNDERSCORE = "_";

	public static String getSymbolSoundStrId(Symbol symbol) {
		if (symbol.getIndex() > 0) {
			if (symbol.isConsonant()) {
				return ALPHABET + UNDERSCORE + CONSONANT + UNDERSCORE
						+ symbol.getIndex();
			} else if (symbol.isVowel()) {
				return ALPHABET + UNDERSCORE + VOWEL + UNDERSCORE
						+ symbol.getIndex();
			}
		}
		return null;
	}

	public static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}

	public static SimpleDateFormat getFormatter(String zone) {
		SimpleDateFormat formatter = getDefaultFormatter();
		formatter.setTimeZone(TimeZone.getTimeZone(zone));
		return formatter;
	}

	public static String dtsCurrentUTC() {
		return getFormatter("UTC").format(System.currentTimeMillis());
	}

	public static SimpleDateFormat getDefaultFormatter() {
		return new SimpleDateFormat(dbDateTimeFormat, Locale.getDefault());
	}

	public static Date dtCurrentUTC() {
		try {
			return getDefaultFormatter().parse(dtsCurrentUTC());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
