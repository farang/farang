package com.iceberg.lctrain2;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;

import com.iceberg.tlm.alphabet.Alphabet;
import com.iceberg.tlm.alphabet.Symbol;
import com.iceberg.tlm.cases.sets.AlphabetCasePackage;

public class MainActivity extends Activity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	public static final String info_option = "com.iceberg.lctrain2.info";

	private Alphabet alphabet = new Alphabet();

	private MediaPlayer player;

	private NavigationDrawerFragment mNavigationDrawerFragment;

	private CharSequence mTitle;

	public Alphabet getAlphabet() {
		return alphabet;
	}

	public AlphabetCasePackage getCasePackage() {
		return getAlphabet().getCasePackage();
	}

	public boolean isConnected() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected())
			return true;
		else
			return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager
				.beginTransaction()
				.replace(
						R.id.container,
						isShowInfo() ? IntroCaseFragment
								.newInstance(position + 1) : CaseFragment
								.newInstance(position + 1)

				).commit();
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	public boolean isShowInfo() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return prefs.getBoolean(info_option, true);
	}

	public void setInfoOption(boolean isShowInfo) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = prefs.edit();
		editor.putBoolean(info_option, isShowInfo);
		editor.apply();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			getMenuInflater().inflate(R.menu.global, menu);
			restoreActionBar();
			MenuItem menuItem = menu.findItem(R.id.action_show_hide_intro);
			menuItem.setChecked(isShowInfo());
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_show_hide_intro) {
			if (item.isChecked()) {
				item.setChecked(false);
				setInfoOption(false);
			} else {
				item.setChecked(true);
				setInfoOption(true);
			}
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public boolean isExistsSymbolSound(Symbol symbol) {
		String strId = Helper.getSymbolSoundStrId(symbol);
		if (strId != null) {
			return getResources().getIdentifier(strId, "raw",
					"com.iceberg.lctrain2") != 0;
		}
		return false;
	}

	public void play(Symbol symbol) {
		if (isExistsSymbolSound(symbol)) {
			if (player != null) {
				player.release();
				player = null;
			}
			player = MediaPlayer.create(
					getApplicationContext(),
					getResources().getIdentifier(
							Helper.getSymbolSoundStrId(symbol), "raw",
							"com.iceberg.lctrain2"));
			player.start();
		}
	}

}
