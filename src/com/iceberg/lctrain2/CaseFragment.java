package com.iceberg.lctrain2;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.iceberg.tlm.cases.AlphabetCase;

public class CaseFragment extends Fragment {

	public static final int STATE_QUESTION = 0;

	public static final int STATE_INCORRECT = 1;

	private int state = STATE_QUESTION;

	private Button mNextButton;

	private TextView mQuestionTextView;

	private TextView mAnswerEditText;

	private TextView mErrorsCounterTextView;

	private TextView mSuccessCounterTextView;

	private static final String ARG_SECTION_NUMBER = "section_number";

	private AlphabetCase tCase;

	private MemStat curMemStat;

	private long questionTime;

	public AlphabetCase getCase() {
		if (tCase == null) {
			return tCase = (AlphabetCase) ((MainActivity) getActivity())
					.getCasePackage().getCases()
					.get(getArguments().getInt(ARG_SECTION_NUMBER) - 1);
		}
		return tCase;
	}

	protected void initMemStat() {
		if (curMemStat != null)
			closeMemStat();

		TelephonyManager telephonyManager = (TelephonyManager) getActivity()
				.getSystemService(Context.TELEPHONY_SERVICE);
		curMemStat = new MemStat(telephonyManager.getDeviceId(), getCase()
				.getClass().getSimpleName(), getCase().getStrategyParams());
	}

	protected void closeMemStat() {
		if (curMemStat != null && ((MainActivity) getActivity()).isConnected()) {
			curMemStat.trySend();
			curMemStat = null;
		}
	}

	protected void updateStats() {
		mSuccessCounterTextView.setText(getCase().getStats().getSuccess() + "");
		mErrorsCounterTextView.setText(getCase().getStats().getErrors() + "");
	}

	protected void updateQuestionString() {
		mQuestionTextView.setText(" " + getCase().getDirectString() + " ");
		questionTime = System.currentTimeMillis();
	}

	protected void updateAll() {
		updateStats();
		updateQuestionString();
		resetAnswer();
	}

	protected void incError() {
		getCase().setWrongResponse();
		updateStats();
	}

	protected void incSuccess() {
		getCase().setRightResponse();
		updateStats();
	}

	protected void next() {
		getCase().next();
	}

	@Override
	public void onDestroy() {
		closeMemStat();
		super.onDestroy();
	}

	@Override
	public void onStart() {
		super.onStart();
		initMemStat();
	}

	@Override
	public void onStop() {
		closeMemStat();
		super.onStop();

	}

	@Override
	public void onResume() {
		super.onResume();
		initMemStat();
	}

	protected void answerSetRightResponse() {
		mAnswerEditText.setText(getString(R.string.right)
				+ getCase().getAssignedString());
		mAnswerEditText.setTextColor(Color.RED);
	}

	protected void resetAnswer() {
		mAnswerEditText.setText("");
		mAnswerEditText.setTextColor(Color.BLACK);
	}

	protected boolean isRightResponse() {
		return getCase().isRightResponse(mAnswerEditText.getText().toString());
	}

	public static CaseFragment newInstance(int sectionNumber) {
		CaseFragment fragment = new CaseFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public CaseFragment() {

	}

	public void updateMemStat(int uniqueIndex, boolean isSuccess, long time) {
		if (curMemStat != null) {
			curMemStat.add(uniqueIndex, isSuccess, time);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_case, container,
				false);

		mErrorsCounterTextView = (TextView) rootView
				.findViewById(R.id.question_errors);
		mSuccessCounterTextView = (TextView) rootView
				.findViewById(R.id.question_success);
		mAnswerEditText = (EditText) rootView
				.findViewById(R.id.answer_editText);
		mQuestionTextView = (TextView) rootView
				.findViewById(R.id.question_textView);
		mNextButton = (Button) rootView.findViewById(R.id.next_button);

		next();

		updateAll();

		mNextButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (state == STATE_QUESTION) {
					if (isRightResponse()) {
						updateMemStat(getCase().getSymbol().getUniqueIndex(),
								true, System.currentTimeMillis() - questionTime);
						incSuccess();
						next();
						updateAll();
						Toast.makeText(getActivity(), R.string.toast_correct,
								Toast.LENGTH_SHORT).show();
					} else {
						updateMemStat(getCase().getSymbol().getUniqueIndex(),
								false, System.currentTimeMillis()
										- questionTime);
						state = STATE_INCORRECT;
						incError();
						updateStats();
						answerSetRightResponse();
						Toast.makeText(getActivity(), R.string.toast_incorrect,
								Toast.LENGTH_SHORT).show();
						mNextButton.setText(R.string.next_button);
					}
				} else {
					next();
					updateAll();
					mNextButton.setText(R.string.check_and_next_button);
					state = STATE_QUESTION;
				}
			}
		});

		mQuestionTextView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainActivity) getActivity()).play(getCase().getSymbol());
			}
		});
		return rootView;
	}

}
