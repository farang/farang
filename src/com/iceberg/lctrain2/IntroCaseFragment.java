package com.iceberg.lctrain2;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.iceberg.tlm.alphabet.Symbol;
import com.iceberg.tlm.cases.AlphabetCase;

public class IntroCaseFragment extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";

	private AlphabetCase tCase;

	private Button mStartButton;

	private TextView mDescriptionTextView;

	private TextView mDescriptionTitleTextView;

	private TextView mDescriptionLettersTextView;

	public AlphabetCase getCase() {
		if (tCase == null) {
			return tCase = (AlphabetCase) ((MainActivity) getActivity())
					.getCasePackage().getCases()
					.get(getArguments().getInt(ARG_SECTION_NUMBER) - 1);
		}
		return tCase;
	}

	public static IntroCaseFragment newInstance(int sectionNumber) {
		IntroCaseFragment fragment = new IntroCaseFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public IntroCaseFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_intro_case,
				container, false);

		mDescriptionTextView = (TextView) rootView
				.findViewById(R.id.description_textView);
		mDescriptionTitleTextView = (TextView) rootView
				.findViewById(R.id.description_title_textView);
		mDescriptionLettersTextView = (TextView) rootView
				.findViewById(R.id.description_letters_textView);
		mStartButton = (Button) rootView.findViewById(R.id.start_button);

		mDescriptionTextView.setText(getCase().getDescription());

		mDescriptionTitleTextView.setText(getCase().getName());

		String letters = "\n";
		for (Symbol symbol : getCase().getSymbols()) {
			letters += symbol.getSign() + "  -  " + symbol.getPronounciation()
					+ "\n";
		}

		mDescriptionLettersTextView.setText(letters);

		mStartButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager
						.beginTransaction()
						.replace(
								R.id.container,
								CaseFragment.newInstance(getArguments().getInt(
										ARG_SECTION_NUMBER))).commit();
			}
		});

		return rootView;
	}

}
