package com.iceberg.lctrain2;

import java.util.HashMap;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;

public class FarangApp extends Application {

	private static final String PROPERTY_ID = "UA-47688574-2";

	public static int GENERAL_TRACKER = 0;

	public enum TrackerName {
		APP_TRACKER, GLOBAL_TRACKER
	}

	HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	public FarangApp() {
		super();
	}

	synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
			Tracker tracker;
			if (trackerId == TrackerName.APP_TRACKER) {
				tracker = analytics.newTracker(PROPERTY_ID);
			} else if (trackerId == TrackerName.GLOBAL_TRACKER) {
				tracker = analytics.newTracker(R.xml.global_tracker);
			} else {
				tracker = analytics.newTracker(R.xml.global_tracker);
			}
			tracker.enableAdvertisingIdCollection(true);
			mTrackers.put(trackerId, tracker);
		}
		return mTrackers.get(trackerId);
	}

}
