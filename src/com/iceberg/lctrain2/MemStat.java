package com.iceberg.lctrain2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class MemStat {

	private String deviceId;

	private String caseName;

	private String strategyParams;

	private Date opened;

	private Date closed;

	private List<MemStatItem> items;

	public MemStat(String deviceId, String caseName, String strategyParams) {
		super();
		this.deviceId = deviceId;
		this.caseName = caseName;
		this.strategyParams = strategyParams;
		this.opened = Helper.dtCurrentUTC();
	}

	private String toJSON() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("device_id", deviceId);
			jsonObject.put("case_name", caseName);
			jsonObject.put("strategy_params", strategyParams);
			jsonObject.put("opened", opened.getTime());
			jsonObject.put("closed", closed.getTime());
			jsonObject.put("data", getData());
			return jsonObject.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void add(int uniqueIndex, boolean isSuccess, long time) {
		if (items == null) {
			items = new ArrayList<MemStatItem>();
		}
		items.add(new MemStatItem(uniqueIndex, time, isSuccess));
	}

	private String getData() {
		if (items == null || items.size() == 0) {
			return null;
		}

		String data = "";
		for (int i = 0; i < items.size(); i++) {
			if (i > 0) {
				data += "#" + items.get(i).toDataString();
			} else {
				data += items.get(i).toDataString();
			}
		}
		return data;
	}

	public void trySend() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if (getData() == null)
					return;
				if (closed == null)
					closed = Helper.dtCurrentUTC();
				try {
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httpPost = new HttpPost(
							"http://siamway.ru:9000/modules/memstat/send");
					String json = toJSON();

					StringEntity stringEntity = new StringEntity(json);
					httpPost.setEntity(stringEntity);

					httpPost.setHeader("Accept", "application/json");
					httpPost.setHeader("Content-type", "application/json");

					httpclient.execute(httpPost);
				} catch (Exception e) {
					Log.d("InputStream", e.toString());
				}
			}
		}).start();
	}

}
